/// <reference path="d.ts/DefinitelyTyped/node/node.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/mongodb.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/q.d.ts" />
import q = require('q');
export interface User {
    login: String;
    password: String;
    todoLists: any[];
}
export declare function findUser(name: String): q.makePromise<User>;
export declare function findOrCreateUser(name: String, password: String): q.makePromise<User>;
export declare function createUser(name: String, password: String): q.makePromise<User>;
