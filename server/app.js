/// <reference path="d.ts/DefinitelyTyped/node/node.d.ts" />
/// <reference path="d.ts/DefinitelyTyped/express/express.d.ts" />
/// <reference path="userRepository.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/q.d.ts" />
var express = require('express');
var http = require('http');
var path = require('path');
var userRepository = require('./userRepository');
var q = require('q');

var everyauth = require('everyauth');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use('/client', express.static(path.join(__dirname, '../client')));

if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}
var users = {};

function getUser(req) {
    console.log('is logged in?', req.loggedIn);
    if (req.loggedIn) {
        console.log('getting user');
        console.log(req.user);
        return userRepository.findUser(req.session.auth.userId);
    } else {
        var deferred = q.defer();
        deferred.reject('Not logged in');
        return deferred.promise;
    }
}

everyauth.everymodule.userPkey('name').findUserById(function (name, cb) {
    console.log('filling user', name);
    userRepository.findUser(name).then(function (user) {
        return cb(user);
    }, function (err) {
        return cb(null);
    });
});
everyauth.password.getLoginPath('/login').postLoginPath('/login').getRegisterPath('/register').postRegisterPath('/register').authenticate(function (login, password) {
    var promise = this.Promise();
    userRepository.findOrCreateUser(login, password).then(function (user) {
        if (user.password !== password) {
            promise.fulfill(['Wrong password']);
        } else {
            promise.fulfill(user);
        }
    });
    return promise;
}).registerUser(function (attrs) {
}).respondToLoginSucceed(function (res, user) {
    console.log('login successful');
    if (user) {
        res.json({ success: true }, 200);
    }
}).respondToLoginFail(function (req, res, errors, login) {
    if (!errors || !errors.length)
        return;
    res.json({ success: false, errors: errors }, 401);
});
app.use(everyauth.middleware(app));

app.use(app.router);

var authorizedRoute = function (authorizedCallback) {
    return function (req, res, next) {
        getUser(req).then(function (user) {
            if (user == null)
                res.send(403, 'Access denied');
else
                authorizedCallback(req, res, next);
        }, function (err) {
            return res.send(403, 'Access denied');
        });
    };
};

app.get('/', authorizedRoute(function (req, res) {
    getUser(req).then(function (user) {
        res.json({ user: user.name, todoLists: user.todoLists });
    });
}));

app.post('/', authorizedRoute(function (req, res) {
    getUser(req).then(function (user) {
        user.todoLists = JSON.parse(req.body.todoLists);
        userRepository.saveUser(user);
        res.send('OK');
    });
}));

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});

