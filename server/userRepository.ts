/// <reference path="d.ts/DefinitelyTyped/node/node.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/mongodb.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/q.d.ts" />

import mongo = require('mongodb');
import q = require('q');

var Db = mongo.Db,
    Server = mongo.Server,
    config = {
        host: 'localhost',
        port: 27017
    };
var userCollection;


export interface User {
    name: String;
    password: String;
    todoLists: Array<any>;
}

export function findUser(login:String):q.makePromise<User> {
    var deferred = q.defer<User>();
    getUserCollection().then((userCollection:mongo.Collection) => {
        userCollection.findOne({login: login}, (err, user) => {
            if (err) deferred.reject(err);
            else deferred.resolve(user);
        });
    }, err => deferred.reject(err));
    return deferred.promise;
}

export function findOrCreateUser (login: String, password: String) {
    var deferred = q.defer<User>();
    findUser(login).then(user => {
        if(user) { deferred.resolve(user); return; }
        createUser(login, password).then(user => deferred.resolve(user), err => deferred.reject(err));
    }, err => deferred.reject(err));
    return deferred.promise;
}

export function createUser(login: String, password: String) {
    var deferred = q.defer<User>();
    getUserCollection().then(userCollection => {
        userCollection.insert({login: login, password: password, todoLists: []}, (err, result) => {
            if(result.length > 0)
                deferred.resolve(result[0]);
            else
                deferred.reject(err);
        });
    });
    return deferred.promise;
}

export function saveUser(user: User) {
    getUserCollection().then((collection:mongo.Collection) => collection.save(user, () => {}));
}

function getUserCollection():q.makePromise<mongo.Collection> {
    var deferred = q.defer<mongo.Collection>();
    if (userCollection)
        deferred.resolve(userCollection);
    else
        new Db('thesis-todo', new Server(config.host, config.port, {}), {native_parser: true, safe: false})
            .open((err, db:mongo.Db) => {
                if (err) {
                    deferred.reject(err);
                    return;
                }
                db.collection('users', (err, collection) => {
                    if (err) deferred.reject(err);
                    else {
                        deferred.resolve(collection);
                        userCollection = collection;
                    }
                });
            });
    return deferred.promise;
}