/// <reference path="d.ts/DefinitelyTyped/node/node.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/mongodb.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/q.d.ts" />
var mongo = require('mongodb');
var q = require('q');

var Db = mongo.Db, Server = mongo.Server, config = {
    host: 'localhost',
    port: 27017
};
var userCollection;

function findUser(name) {
    console.log('finding user', name);
    var deferred = q.defer();
    getUserCollection().then(function (userCollection) {
        userCollection.findOne({ name: name }, function (err, user) {
            if (err)
                deferred.reject(err);
else
                deferred.resolve(user);
        });
    }, function (err) {
        return deferred.reject(err);
    });
    return deferred.promise;
}
exports.findUser = findUser;

function findOrCreateUser(name, password) {
    var deferred = q.defer();
    exports.findUser(name).then(function (user) {
        if (user) {
            deferred.resolve(user);
            return;
        }
        exports.createUser(name, password).then(function (user) {
            return deferred.resolve(user);
        }, function (err) {
            return deferred.reject(err);
        });
    }, function (err) {
        return deferred.reject(err);
    });
    return deferred.promise;
}
exports.findOrCreateUser = findOrCreateUser;

function createUser(name, password) {
    var deferred = q.defer();
    getUserCollection().then(function (userCollection) {
        userCollection.insert({ name: name, password: password, todoLists: [] }, function (err, result) {
            if (result.length > 0)
                deferred.resolve(result[0]);
else
                deferred.reject(err);
        });
    });
    return deferred.promise;
}
exports.createUser = createUser;

function saveUser(user) {
    getUserCollection().then(function (collection) {
        return collection.save(user, function () {
        });
    });
}
exports.saveUser = saveUser;

function getUserCollection() {
    var deferred = q.defer();
    if (userCollection)
        deferred.resolve(userCollection);
else
        new Db('thesis-todo', new Server(config.host, config.port, {}), { native_parser: true, safe: false }).open(function (err, db) {
            if (err) {
                deferred.reject(err);
                return;
            }
            db.collection('users', function (err, collection) {
                if (err)
                    deferred.reject(err);
else {
                    deferred.resolve(collection);
                    userCollection = collection;
                }
            });
        });
    return deferred.promise;
}

