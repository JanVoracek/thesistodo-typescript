declare module "q" {
	export class deferred<T> {
		promise : makePromise<T>;
		public resolve(resolvedValue: T) : any;
		public reject(exception) : any;
		public notify() : void;
	}

	export function defer<T>() : deferred<T>;

	export class makePromise<T> {
		constructor (descriptor, fallback, valueOf, exception);

		public then(fulfilled?:fulfillCallback<T> , rejected? , progressed? ) : makePromise<T>;

		public isResolved() : boolean;
		public isFulfilled() : boolean;
		public isRejected() : boolean;
        public when(fulfilled?:fulfillCallback<T>, rejected?, progressed?) : makePromise<T>;
		public spread(fulfilled?:fulfillCallback<T>, rejected?) : makePromise<T>;
		public send(op) : makePromise<T>;
        public get() : makePromise<T>;
		public put() : makePromise<T>;
		public del() : makePromise<T>;
        public post() : makePromise<T>;
		public invoke(name/*, args...*/);
        public keys();
        public apply();
		public call();
		public bind();
        public fapply();
		public fcall();
		public fbind();
        public all();
		public allResolved();
        public view();
		public viewInfo();
        public timeout();
		public delay();
        public catch();
		public finally();
		public fail();
		public fin();
		public progress();
		public end();
        public ncall();
		public napply();
		public nbind();
        public npost();
		public ninvoke();
        public nend();
	}

	export function when<T>(promise : makePromise<T>, fulfilled, rejected, progressed) : makePromise<T>;

    export interface fulfillCallback<T> {
        (fulfilledValue: T)
    }
}