/// <reference path="d.ts/DefinitelyTyped/node/node.d.ts" />
/// <reference path="d.ts/DefinitelyTyped/express/express.d.ts" />
/// <reference path="userRepository.d.ts" />
/// <reference path="d.ts/typescript-node-definitions/q.d.ts" />

import express = require('express');
import http = require('http');
import path = require('path');
import userRepository = require('./userRepository');
import q = require('q');

var everyauth = require('everyauth');

var app:Express = <any>express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use('/client/', express.static(path.join(__dirname, '../client')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}
var users = {};

function getUser(req):q.makePromise<userRepository.User> {
    if (req.loggedIn) {
        return userRepository.findUser(req.session.auth.userId);
    } else {
        var deferred = q.defer<userRepository.User>();
        deferred.reject('Not logged in');
        return deferred.promise;
    }
}

everyauth.everymodule
    .userPkey('login')
    .findUserById(function(login, cb) {
        userRepository.findUser(login).then(user => cb(user), err => cb(null));
    });
everyauth.password
    .getLoginPath('/login')
    .postLoginPath('/login')
    .getRegisterPath('/register')
    .postRegisterPath('/register')
    .authenticate(function(login, password) {
        var promise = this.Promise();
        userRepository.findOrCreateUser(login, password).then(user => {
            if (user.password !== password) {
                promise.fulfill(['Wrong password']);
            } else {
                promise.fulfill(user);
            }
        });
        return promise;
    })
    .registerUser(function (attrs) {

    })
    .respondToLoginSucceed(function (res, user) {
        if (user) { /* Then the login was successful */
            res.json({ success: true }, 200);
        }
    })
    .respondToLoginFail(function (req, res, errors, login) {
        if (!errors || !errors.length) return;
        res.json({ success: false, errors: errors }, 401);
    });

everyauth.facebook
    .appId('585883654788924')
    .appSecret('373915d4bb842fdc2388ac54c23912ff')
    .entryPath('/login/facebook')
    .findOrCreateUser(function(session, accessToken, _, fbUserMeta) => {
        var promise = this.Promise();
        userRepository.findOrCreateUser(fbUserMeta.username, '').then(user => {
            promise.fulfill(user);
        });
        return promise;
    })
    .redirectPath('/index.html');

app.use(everyauth.middleware(app));

app.use(app.router);

var authorizedRoute = (authorizedCallback) => {
    return (req, res, next) => {
        getUser(req).then(user => {
            if(user == null)
                res.send(403, 'Access denied');
            else
                authorizedCallback(req, res, next);
        }, err => res.send(403, 'Access denied'));
    }
};

app.get('/', authorizedRoute((req, res) => {
    getUser(req).then(user => {
        res.json({user: user.login, todoLists: user.todoLists});
    });
}));

app.post('/', authorizedRoute((req, res) => {
    getUser(req).then(user => {
        user.todoLists = JSON.parse(req.body.todoLists);
        userRepository.saveUser(user);
        res.json({success: true});
    });
}));

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
