/// <reference path="../d.ts/DefinitelyTyped/knockout/knockout.d.ts" />
/// <reference path="../d.ts/DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../d.ts/DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../d.ts/DefinitelyTyped/knockout.mapping/knockout.mapping.d.ts" />

var initializationInProgress = true;

ko.extenders['persist'] = (target, option) => {
    target.subscribe(newValue => {
        if (!initializationInProgress)
            storage.save(todoAppViewModel);
    });
    return target;
};

class ViewModelStorage {
    private previousValue:string;

    constructor(private url:string) {
    }

    save(model:TodoAppViewModel) {
        var serializedTodoLists = ko.toJSON(model.todoLists);
        if (this.previousValue !== serializedTodoLists)
            $.post(this.url, { todoLists: serializedTodoLists });
        this.previousValue = serializedTodoLists;
    }

    fill(model:TodoAppViewModel) {
        var promise = jQuery.Deferred();

        this.loadModel()
            .then(response => {
                this.fillModel(model, response);
                promise.resolve()
            })
            .fail(() => this.tryToAuthenticate(promise, () => {
                this.fill(model)
            }));

        return promise;
    }

    private loadModel() {
        return $.get(this.url);
    }

    private fillModel(model:TodoAppViewModel, response) {
        var userName = response.user;
        var todoListsPlain = response.todoLists;

        model.user(userName);

        if (ko.toJSON(todoListsPlain) == ko.toJSON(model.todoLists)) {
            return;

        }
        var todoLists = todoListsPlain.map(TodoList.createList);
        model.todoLists(todoLists);
        model.selectedTodoList(todoLists[0]);
    }

    private tryToAuthenticate(promise, cb) {
        var $loginForm = $("#login-form");
        var $error = $loginForm.find('.error');
        $error.text('');

        $('#login-dialog').dialog('open');
        $loginForm.on('submit', e => {
            e.preventDefault();

            var login = $('#login').val();
            var password = $('#password').val();
            var authRequest = $.post('/login', {login: login, password: password});
            authRequest.fail(err => {
                $error.text(err.responseJSON.errors[0]);
            });
            authRequest.pipe(cb)
                .then(() => {
                    promise.resolve();
                    $('#login-dialog').dialog('close');
                    $loginForm.off('submit');
                })
                .fail(() => promise.reject());
        });
    }
}

module kop {
    export function observable(name:string, value?:any) {
        return ko.observable(value).extend({persist: name});
    }

    export function observableArray(name:string) {
        return ko.observableArray().extend({persist: name});
    }
}

interface TodoListPlainObject {
    name: string;
    todos: TodoPlainObject[];
}

interface TodoPlainObject {
    content: string;
    done: boolean;
}

class Todo {
    task = kop.observable('task');
    done = kop.observable('done');

    constructor(task = 'some todo...', done = false) {
        this.task(task);
        this.done(done);
    }
}

class TodoList {
    name = kop.observable('name-list');
    todos = kop.observableArray('todos');
    removeTodo:(todo:Todo) => void;
    clearDone:() => void;


    constructor(name:string = '') {
        this.name(name);
        this.todos.remaining = ko.computed(() =>
                this.todos().filter(todo => !todo.done()).length
        );

        this.todos.done = ko.computed(() =>
                this.todos().filter(todo => todo.done()).length
        );

        this.removeTodo = todo => this.todos.remove(todo); // Knockout hack to save "this" reference
        this.clearDone = () => this.todos.remove(todo => todo.done());
    }

    addTodo(todo:Todo) {
        this.todos.push(todo);
    }

    static createList(obj:TodoListPlainObject) {
        var todoList = new TodoList(obj.name);
        var todos = obj.todos.map(todoPlain => new Todo(todoPlain.task, todoPlain.done));
        todoList.todos(todos);
        return todoList;
    }
}

class TodoAppViewModel {
    todoLists = kop.observableArray('todoLists');
    selectedTodoList = ko.observable();
    user = ko.observable();

    deleteList(todoList:TodoList) {
        this.todoLists.remove(this.selectedTodoList());
        this.selectedTodoList(this.todoLists[0]);
    }

    clear() {
        this.todoLists([]);
        this.selectedTodoList(undefined);
        this.user(undefined);
    }
}

function fillModel() {
    storage.fill(todoAppViewModel).then(() => {
        initializationInProgress = false;
    });
}

var todoAppViewModel:TodoAppViewModel;
var storage = new ViewModelStorage((<any>window).location.origin);

$(() => {
    $('#login-dialog').dialog({
        modal: true,
        autoOpen: false,
        dialogClass: 'no-close',
        closeOnEscape: false,
        close: () => {
            $('#password').val('')
        }
    });
    $('#logout').on('click', () => {
        $.get('/logout')
            .always(() => {
                initializationInProgress = true;
                todoAppViewModel.clear();
                fillModel();
            });
    });

    todoAppViewModel = new TodoAppViewModel();
    fillModel();

    ko.applyBindings(todoAppViewModel);

    var $todoapp = $('#todoapp');
    $todoapp.on('dblclick', '#todo-list .todo', function () {
        $(this).addClass('editing');
        $(this).find('.todo-input').focus();
    });

    $todoapp.on('keyup', '#todo-list .todo-input', function (event:KeyboardEvent) {
        if (event.keyCode == 13)
            $(this).parents('.todo').removeClass('editing');
        if (event.keyCode == 27) {
            $(this).val($(this).parents('.todo').find('.todo-content').text());
            $(this).parents('.todo').removeClass('editing');
        }
    });

    $todoapp.on('keyup', '#new-todo', function (event:KeyboardEvent) {
        if (event.keyCode == 13) {
            todoAppViewModel.selectedTodoList().addTodo(new Todo($(this).val()));
            $(this).val('');
        }
        if (event.keyCode == 27) {
            $(this).val('');
        }
    });

    $todoapp.on('change', '.mark-all-done', function () {
        var done = $(this).is(':checked');
        $.each(todoAppViewModel.selectedTodoList().todos(), (_, todo) => {
            todo.done(done);
        });
    });

    $('#list-selector').on('click', '.list-add', function () {
        $('#list-selector').addClass('editing');
        $('#new-list').focus();
    });

    $('#new-list').on('keyup', function (event:KeyboardEvent) {
        if (event.keyCode == 13) {
            var todoList = new TodoList($(this).val());
            todoAppViewModel.todoLists.push(todoList);
            todoAppViewModel.selectedTodoList(todoList);
            $('#list-selector').removeClass('editing');
            $(this).val('');
        }
        if (event.keyCode == 27) {
            $('#list-selector').removeClass('editing');
            $(this).val('');
        }
    });
});