var _this = this;
var initializationInProgress = true;

ko.extenders['persist'] = function (target, option) {
    target.subscribe(function (newValue) {
        if (!initializationInProgress)
            storage.save(todoAppViewModel);
    });
    return target;
};

var ViewModelStorage = (function () {
    function ViewModelStorage(url) {
        this.url = url;
    }
    ViewModelStorage.prototype.save = function (model) {
        var serializedTodoLists = ko.toJSON(model.todoLists);
        if (this.previousValue !== serializedTodoLists)
            $.post(this.url, { todoLists: serializedTodoLists });
        this.previousValue = serializedTodoLists;
    };

    ViewModelStorage.prototype.fill = function (model) {
        var _this = this;
        var promise = jQuery.Deferred();

        this.loadModel().then(function (response) {
            _this.fillModel(model, response);
            promise.resolve();
        }).fail(function (jqXHR, status, error) {
            return _this.tryToAuthenticate(promise, function () {
                _this.fill(model);
            });
        });

        return promise;
    };

    ViewModelStorage.prototype.loadModel = function () {
        return $.get(this.url);
    };

    ViewModelStorage.prototype.fillModel = function (model, response) {
        var userName = response.user;
        var todoListsPlain = response.todoLists;

        model.user(userName);

        if (ko.toJSON(todoListsPlain) == ko.toJSON(model.todoLists)) {
            return;
        }
        var todoLists = todoListsPlain.map(TodoList.createList);
        model.todoLists(todoLists);
        model.selectedTodoList(todoLists[0]);
    };

    ViewModelStorage.prototype.tryToAuthenticate = function (promise, cb) {
        var $loginForm = $("#login-form");
        var $error = $loginForm.find('.error');
        $error.text('');

        $('#login-dialog').dialog('open');
        $loginForm.on('submit', function (e) {
            e.preventDefault();

            var login = $('#login').val();
            var password = $('#password').val();
            var authRequest = $.post('/login', { login: login, password: password });
            authRequest.fail(function (err) {
                $error.text(err.responseJSON.errors[0]);
            });
            authRequest.pipe(cb).then(function () {
                promise.resolve();
                $('#login-dialog').dialog('close');
                $loginForm.off('submit');
            }).fail(function () {
                return promise.reject();
            });
        });
    };
    return ViewModelStorage;
})();

var kop;
(function (kop) {
    function observable(name, value) {
        return ko.observable(value).extend({ persist: name });
    }
    kop.observable = observable;

    function observableArray(name) {
        return ko.observableArray().extend({ persist: name });
    }
    kop.observableArray = observableArray;
})(kop || (kop = {}));

var Todo = (function () {
    function Todo(content, done) {
        if (typeof content === "undefined") { content = 'some todo...'; }
        if (typeof done === "undefined") { done = false; }
        this.content = kop.observable('content');
        this.done = kop.observable('done');
        this.content(content);
        this.done(done);
    }
    return Todo;
})();

var TodoList = (function () {
    function TodoList(name) {
        if (typeof name === "undefined") { name = ''; }
        var _this = this;
        this.name = kop.observable('name-list');
        this.todos = kop.observableArray('todos');
        this.name(name);
        this.todos.remaining = ko.computed(function () {
            return _this.todos().filter(function (todo) {
                return !todo.done();
            }).length;
        });

        this.todos.done = ko.computed(function () {
            return _this.todos().filter(function (todo) {
                return todo.done();
            }).length;
        });

        this.removeTodo = function (todo) {
            return _this.todos.remove(todo);
        };
        this.clearDone = function () {
            return _this.todos.remove(function (todo) {
                return todo.done();
            });
        };
    }
    TodoList.prototype.addTodo = function (todo) {
        this.todos.push(todo);
    };

    TodoList.createList = function (obj) {
        var todoList = new TodoList(obj.name);
        var todos = obj.todos.map(function (todoPlain) {
            return new Todo(todoPlain.content, todoPlain.done);
        });
        todoList.todos(todos);
        return todoList;
    };
    return TodoList;
})();

var TodoAppViewModel = (function () {
    function TodoAppViewModel() {
        this.todoLists = kop.observableArray('todoLists');
        this.selectedTodoList = ko.observable();
        this.user = ko.observable();
    }
    TodoAppViewModel.prototype.deleteList = function (todoList) {
        this.todoLists.remove(this.selectedTodoList());
        this.selectedTodoList(this.todoLists[0]);
    };

    TodoAppViewModel.prototype.clear = function () {
        this.todoLists([]);
        this.selectedTodoList(undefined);
        this.user(undefined);
    };
    return TodoAppViewModel;
})();

function fillModel() {
    storage.fill(todoAppViewModel).then(function () {
        initializationInProgress = false;
    });
}

var todoAppViewModel;
var storage = new ViewModelStorage((window).location.origin);

$(function () {
    $('#login-dialog').dialog({
        modal: true,
        autoOpen: false,
        dialogClass: 'no-close',
        closeOnEscape: false,
        close: function () {
            $('#password').val('');
        }
    });
    $('#logout').on('click', function () {
        $.get('/logout').always(function () {
            initializationInProgress = true;
            todoAppViewModel.clear();
            fillModel();
        });
    });

    todoAppViewModel = new TodoAppViewModel();
    fillModel();

    ko.applyBindings(todoAppViewModel);

    var $todoapp = $('#todoapp');
    $todoapp.on('dblclick', '#todo-list .todo', function () {
        $(this).addClass('editing');
        $(this).find('.todo-input').focus();
    });

    $todoapp.on('keyup', '#todo-list .todo-input', function (event) {
        if (event.keyCode == 13)
            $(this).parents('.todo').removeClass('editing');
        if (event.keyCode == 27) {
            $(this).val($(this).parents('.todo').find('.todo-content').text());
            $(this).parents('.todo').removeClass('editing');
        }
    });

    $todoapp.on('keyup', '#new-todo', function (event) {
        if (event.keyCode == 13) {
            todoAppViewModel.selectedTodoList().addTodo(new Todo($(this).val()));
            $(this).val('');
        }
        if (event.keyCode == 27) {
            $(this).val('');
        }
    });

    $todoapp.on('change', '.mark-all-done', function () {
        var done = $(this).is(':checked');
        $.each(todoAppViewModel.selectedTodoList().todos(), function (_, todo) {
            todo.done(done);
        });
    });

    $('#list-selector').on('click', '.list-add', function () {
        $('#list-selector').addClass('editing');
        $('#new-list').focus();
    });

    $('#new-list').on('keyup', function (event) {
        if (event.keyCode == 13) {
            var todoList = new TodoList($(this).val());
            todoAppViewModel.todoLists.push(todoList);
            todoAppViewModel.selectedTodoList(todoList);
            $('#list-selector').removeClass('editing');
            $(this).val('');
        }
        if (event.keyCode == 27) {
            $('#list-selector').removeClass('editing');
            $(this).val('');
        }
    });
});
//@ sourceMappingURL=todos.js.map
